Before do
    current_window.maximize
    @login = Login.new
    @produto = Produto.new
    @cadastroEndereco = CadastroEndereco.new
    @confirmarPagamento = ConfirmarPagamento.new
    @pedidoGerado = PedidoGerado.new

    
end

After do
    shot_file = page.save_screenshot("log/screenshot.png")
    shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
    #atach(shot_b64, "image/png", "Screenshot") # Cucumber anexa o screenshot no report
    
end

