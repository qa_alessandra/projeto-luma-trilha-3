class ConfirmarPagamento 
    
    include Capybara::DSL
    include RSpec::Matchers

    def initialize
        @botao_place_order = EL['botao_place_order']
    end

    def confirmarPagamento
        find(@botao_place_order).click
        sleep 3
        
    end

end


