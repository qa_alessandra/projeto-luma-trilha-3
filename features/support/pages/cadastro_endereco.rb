class CadastroEndereco

    include Capybara::DSL
    include RSpec::Matchers

    def initialize
        @campo_first_name   = EL['campo_first_name']
        @campo_last_name    = EL['campo_last_name']
        @campo_street       = EL['campo_street']
        @campo_country      = EL['campo_country']
        @campo_city         = EL['campo_city']
        @campo_state        = EL['campo_state']
        @campo_postal_code  = EL['campo_postal_code']
        @campo_telephone    = EL['campo_telephone']
        @tipo_envio         = EL['tipo_envio']
        @botao_continue     = EL['botao_continue']
        @class_mask         = EL['class_mask']

        #Dados pessoais 
        @first_name = DATA['first_name']
        @last_name = DATA['last_name']

        #Endereco
        @street_address     = DATA['street_address']
        @city               = DATA['city']
        @state              = DATA['state']
        @postal_code        = DATA['postal_code']
        @phone_number       = DATA['phone_number']                    
    end

    def confirmarEndereco 
        assert_no_selector(@class_mask, wait: 10)
        verifica_endereco = has_css?(@campo_first_name, wait: 3)

        if verifica_endereco == true 
            find(@campo_first_name).set @first_name
            find(@campo_last_name).set @last_name
            find(@campo_street).set @street_address 
            find(@campo_city).set @city 
            find(@campo_country).click
            find(@campo_state).click
            find(@campo_postal_code).set @postal_code
            find(@campo_telephone).set @phone_number
        end
    end
    
    def formasEnvio
        assert_no_selector(@class_mask, wait: 10)
        first(@tipo_envio).click
        find(@botao_continue).click  
        sleep 5
    end
   
end 

