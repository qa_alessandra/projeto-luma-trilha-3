
class PedidoGerado
    include Capybara::DSL
    include RSpec::Matchers


    def initialize
        @mensagem_confirmacao_pedido = EL['mensagem_confirmacao_pedido'] 
    end

    def confirmarPedidoGerado
        expect(find(EL["titulo_pagina"]).text).to eql @mensagem_confirmacao_pedido
    end
end

