class Produto

    include Capybara::DSL
    include RSpec::Matchers

    def initialize
        @imagem = EL['radiant_tee']
        @tamanho = EL['size']
        @cor = EL['cor']
        @botao_adicionar_carrinho = EL['botao_adicionar_carrinho']
        @link_carrinho = EL['link_carrinho']
        @botao_checkout = EL['botao_checkout']
    end

    def selecionarProduto
        find(@imagem).click    
    end

    def selecionarCaracteristicasProduto
        find(@tamanho).click
        find(@cor).click   
    end

    def adicionarProdutoCarrinho
        find(@botao_adicionar_carrinho).click        
    end

    def acessarCarrinho
        click_link(@link_carrinho)
    end

    def acessarCheckout
        sleep 5
        find(@botao_checkout).click   
    end
end



