class Login
    include Capybara::DSL
    include RSpec::Matchers


    def initialize
        @campo_email = EL['campo_email']
        @campo_senha = EL['campo_senha']
        @botao_logar = EL['botao_logar']
    end 

    def visitarPagina(link_entrar)
        click_link(link_entrar)
    end


    def fazerLogin(login, senha)
        find(@campo_email).set(login)
        find(@campo_senha).set(senha)  
    end


    def logar
        find(@botao_logar).click
    end

end