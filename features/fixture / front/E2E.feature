#language:pt

Funcionalidade: Fluxo E2E

@teste
Cenário: Gerar pedidos


Dado que acesso a pagina 'projeto_luma' e pagina de login 'Sign In'
E faço o login com 'roni_cost@example.com' e senha 'roni_cost3@example.com'
E clico em 'Sign In'
Quando pesquisar e adicionar um produto no carrinho
E ir para o Checkout
E confimar endereço e metodo de envio 
E confirmar forma de pagamento
Então pedido é gerado com sucesso 
