
Quando('pesquisar e adicionar um produto no carrinho') do
    @produto.selecionarProduto
    @produto.selecionarCaracteristicasProduto
    @produto.adicionarProdutoCarrinho
    @produto.acessarCarrinho
end
  
Quando('ir para o Checkout') do
    @produto.acessarCheckout
end
  