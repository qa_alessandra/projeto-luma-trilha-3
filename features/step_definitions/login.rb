#visitando a pagina e logando

Dado('que acesso a pagina {string} e pagina de login {string}') do |projeto_luma, link_entrar|
    url = DATA[projeto_luma]
    visit(url)
    @login.visitarPagina(link_entrar)
  end
  
Dado('faço o login com {string} e senha {string}') do |login, senha|
  @login.fazerLogin(login, senha)
end

Dado('clico em {string}') do |logar|
 @login.logar
end

